# Codigos de Ayuda

Hola este es el primero de una serie de código que pueden te pueden ayudar

## Getting Started

Esta vez te muestro como puedes hacer un TryExcept anidado en un decorador con el que puedes controlar
todas las excepciones que desees sin la necesidad de escribir reiteradas veces el código

### Prerequisites
```
python3.7 o docker CE
```

### Installing
```
clone https://gitlab.com/felipesantander.90/codigosayuda.git
```
```
cd codigosayuda
```
```
docker build -t tutorial .
```
## Running App

```
docker run -v $PWD:/programas tutorial
```

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Felipe Santander** 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details




