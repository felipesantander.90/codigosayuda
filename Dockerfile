FROM python:3.7
WORKDIR /programas
COPY requirements.txt .
RUN pip3 install --upgrade pip -r ./requirements.txt
CMD [ "python", "./tryDecorator.py" ]
