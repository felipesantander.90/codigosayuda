import sys
import traceback
def tryExcept(func):
    def funcTry(*args, **kwargs): 
        try:
            return func(*args, **kwargs)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname= traceback.extract_tb(exc_tb)[1][0]
            lineaError=traceback.extract_tb(exc_tb)[1][1]," -"+traceback.extract_tb(exc_tb)[1][3]
            metodo= traceback.extract_tb(exc_tb)[1][2]
            print(f"traceback completo: {traceback.extract_tb(exc_tb)}")
            print(f"nombre: {fname},linea del error: {lineaError}, metodo ejecutado: {metodo}")
    return funcTry

@tryExcept
def test():
    int(None)

if __name__ == "__main__":
    test()
